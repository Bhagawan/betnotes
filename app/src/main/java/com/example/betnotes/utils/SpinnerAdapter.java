package com.example.betnotes.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.betnotes.R;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private ArrayList<String> names = new ArrayList<>();

    public SpinnerAdapter(@NonNull Context context, @NonNull ArrayList<String> objects) {
        super(context, 0 , objects);
        mContext = context;
        names = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = LayoutInflater.from(mContext).inflate(R.layout.spinner_item, parent, false);
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.spinner_item, parent, false);
        String name = names.get(position);
        TextView textView = listItem.findViewById(R.id.spinner_out_text);
        textView.setText(name);
        return listItem;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItem = LayoutInflater.from(mContext).inflate(R.layout.spinner_item2, parent, false);
        String name = names.get(position);
        TextView textView = listItem.findViewById(R.id.text1);
        textView.setText(name);

        return listItem;
    }

    @Override
    public int getCount() {
        if(names.isEmpty()) return 0;
        else return names.size();
    }
}
