package com.example.betnotes.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.betnotes.R;
import com.example.betnotes.database.Bet;
import com.example.betnotes.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class BetItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int HEADER_VIEW = 1;
    private ArrayList<Bet> bets;
    private DatabaseHelper databaseHelper;
    private ItemClickListener mClickListener;
    private ArrayList<Integer> expanded = new ArrayList<>();

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public BetItemsAdapter(ArrayList<Bet> bets, DatabaseHelper databaseHelper) {
        this.bets = bets;
        this.databaseHelper =  databaseHelper;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView betExpandButton;
        private TextView betName;
        private TextView betOdd;
        private TextView betAmount;
        private TextView betProfit;
        private ImageButton betSuccess;
        private ImageButton betFailure;
        private ImageButton betWait;
        private ImageButton betDelete;
        private final ConstraintLayout constraintLayout;
        private LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            betExpandButton = (ImageView) itemView.findViewById(R.id.bet_expand);
            betName = (TextView) itemView.findViewById(R.id.bet_name);
            betOdd = (TextView) itemView.findViewById(R.id.bet_odd);
            betAmount = (TextView) itemView.findViewById(R.id.bet_amount);
            betProfit = (TextView) itemView.findViewById(R.id.bet_profit);
            betSuccess = (ImageButton) itemView.findViewById(R.id.bet_success_button);
            betFailure = (ImageButton) itemView.findViewById(R.id.bet_failure_button);
            betWait = (ImageButton) itemView.findViewById(R.id.bet_ongoing_button);
            betDelete = (ImageButton) itemView.findViewById(R.id.bet_delete_button);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.linearLayout);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.bet_layout);
        }

        public void bindViewMainHolder(int pos, List<Object> payloads) {
            if(!payloads.isEmpty()) {
                if (payloads.get(0) instanceof Integer) {
                    changeProfit(betProfit,(int) payloads.get(0));
                }
            }else {
                bindViewMainHolder(pos);
            }
        }

        public void bindViewMainHolder(int pos) {
            int finalPos = pos - 1;
            int arrayPos = pos;
            Bet bet = bets.get(finalPos);

            betName.setText(bet.getName());
            betAmount.setText(String.valueOf(bet.getAmount()));
            betOdd.setText(String.valueOf(bet.getOdd()));
            int v = getProfit(bet);
            changeProfit(betProfit, v);

            if(isExpanded(bet.getId())){ expand(constraintLayout);}

            betSuccess.setOnClickListener(v1 -> {
                bet.setProfit(1);
                databaseHelper.updateBet(bet);
                notifyItemChanged(arrayPos,getProfit(bet));
                mClickListener.onItemClick(v1,arrayPos);
            });

            betDelete.setOnClickListener(v12 -> {
                databaseHelper.deleteBet(bet);
                bets.remove(finalPos);
                notifyDataSetChanged();
                mClickListener.onItemClick(v12,arrayPos);
            });

            betFailure.setOnClickListener(v13 -> {

                bet.setProfit(-1);
                databaseHelper.updateBet(bet);
                notifyItemChanged(arrayPos,getProfit(bet));
                mClickListener.onItemClick(v13,arrayPos);
            });
            betWait.setOnClickListener(v14 -> {
                bet.setProfit(0);
                databaseHelper.updateBet(bet);
                notifyItemChanged(arrayPos,getProfit(bet));
                mClickListener.onItemClick(v14, arrayPos);
            });

            linearLayout.setOnClickListener(v15 -> {
                toggle(constraintLayout,bet.getId(),betExpandButton);
            });
            betExpandButton.setOnClickListener(view -> {
                toggle(constraintLayout,bet.getId(),betExpandButton);
            });
        }

    }
    public class MainViewHolder extends ViewHolder {
        public MainViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class HeaderViewHolder extends ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER_VIEW) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bet_header_view, parent, false);

            return new HeaderViewHolder(v);
        }

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bet_view, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        try {
            if (holder instanceof MainViewHolder) {
                MainViewHolder vh = (MainViewHolder) holder;
                vh.bindViewMainHolder(position, payloads);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof MainViewHolder) {
                MainViewHolder vh = (MainViewHolder) holder;
                vh.bindViewMainHolder(position);
            } else if (holder instanceof HeaderViewHolder) {
                HeaderViewHolder vh = (HeaderViewHolder) holder;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if(!bets.isEmpty()) return bets.size() + 1;
        else return 0;
    }
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_VIEW;
        }
        return super.getItemViewType(position);
    }

    private int getProfit(Bet bet) {
        int p = 0;
        if(bet.getProfit() > 0) {
            p += (bet.getAmount()* bet.getOdd());
        } else if(bet.getProfit() < 0)p -= bet.getAmount();
        return p;
    }

    private void toggle(ConstraintLayout constraintLayout, int betId, ImageView iv) {
        if(constraintLayout.getVisibility() == View.GONE){
            rotate(iv,0f,180f);
            setAllParentsClip(constraintLayout,false);
            constraintLayout.setVisibility(View.VISIBLE);
            expanded.add(betId);
            Animation sh = AnimationUtils.loadAnimation(constraintLayout.getContext().getApplicationContext(), R.anim.show);
            constraintLayout.startAnimation(sh);
        }
        else {
            rotate(iv,180f,0f);
            setAllParentsClip(constraintLayout,true);
            constraintLayout.setVisibility(View.GONE);
            for(int i = 0; i < expanded.size(); i++){
                if (expanded.get(i) == betId) expanded.remove(i);
            }
        }
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private boolean isExpanded(int id) {
        if(!expanded.isEmpty()) {
            for(int i = 0; i < expanded.size(); i++) {
                if(expanded.get(i) == id) return true;
            }
        }
        return false;
    }

    private void expand(ConstraintLayout constraintLayout) {
        if(constraintLayout.getVisibility() == View.GONE){
            constraintLayout.setVisibility(View.VISIBLE);
        }
    }
    private void changeProfit(TextView profitTextView, int profit) {
        if(profit > 0) profitTextView.setBackgroundTintList(AppCompatResources.getColorStateList(profitTextView.getContext(), R.color.green));
        else if(profit == 0) profitTextView.setBackgroundTintList(AppCompatResources.getColorStateList(profitTextView.getContext(), R.color.grey));
        else  profitTextView.setBackgroundTintList(AppCompatResources.getColorStateList(profitTextView.getContext(), R.color.red));
        profitTextView.setText(String.valueOf(profit));
    }

    private static void setAllParentsClip(View v, boolean enabled) {
        while (v.getParent() != null && v.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) v.getParent();
            viewGroup.setClipChildren(enabled);
            viewGroup.setClipToPadding(enabled);
            v = viewGroup;
        }
    }

    private void rotate(ImageView v,float from, float to){
        RotateAnimation rotateAnimation = new RotateAnimation(from,to,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        rotateAnimation.setDuration(400);
        rotateAnimation.setFillAfter(true);
        v.startAnimation(rotateAnimation);
    }
}
