package com.example.betnotes.utils;


import android.widget.PopupWindow;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.ListPopupWindow;

import java.lang.reflect.Field;

public class ImmSpin {
    public static void avoidSpinnerDropdownFocus(androidx.appcompat.widget.AppCompatSpinner spinner) {
        try {
            Field listPopupField = androidx.appcompat.widget.AppCompatSpinner.class.getDeclaredField("mPopup");
            listPopupField.setAccessible(true);
            Object listPopup = listPopupField.get(spinner);
            if (listPopup instanceof ListPopupWindow) {
                Field popupField = ListPopupWindow.class.getDeclaredField("mPopup");
                popupField.setAccessible(true);
                Object popup = popupField.get((ListPopupWindow) listPopup);
                if (popup instanceof PopupWindow) {
                    ((PopupWindow) popup).setFocusable(false);
                }
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
