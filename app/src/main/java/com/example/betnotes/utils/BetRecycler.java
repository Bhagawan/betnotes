package com.example.betnotes.utils;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.betnotes.R;
import com.example.betnotes.database.Bet;
import com.example.betnotes.database.DatabaseHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BetRecycler  extends RecyclerView.Adapter<BetRecycler.ViewHolder>{
    private ArrayList<String> months;
    private DatabaseHelper databaseHelper;
    private String bankroll;
    private Animation showAnimation, hideAnimation;
    private static int currentPosition = 0;
    private Callback callback;
    private Context context;

    public interface Callback {
        void updateAccount();
    }

    public BetRecycler(Context context, String bankroll) {
        this.context = context;
        this.databaseHelper = new DatabaseHelper(context);
        this.bankroll = bankroll;
        this.months = databaseHelper.getMonths(bankroll);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView monthExpandImage;
        private TextView monthMonth;
        private TextView monthMonthTotal;
        private RecyclerView recyclerView;

        public ViewHolder(final View itemView) {
            super(itemView);

            monthExpandImage = (ImageView) itemView.findViewById(R.id.month_expand_button);
            monthMonth = (TextView) itemView.findViewById(R.id.month_output);
            monthMonthTotal = (TextView) itemView.findViewById(R.id.month_total);
            recyclerView = (RecyclerView)  itemView.findViewById(R.id.month_recycler);
            showAnimation = AnimationUtils.loadAnimation( recyclerView.getContext(), R.anim.show);
            hideAnimation = AnimationUtils.loadAnimation(recyclerView.getContext(), R.anim.hide);

        }
    }

    @NonNull
    @Override
    public BetRecycler.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.day_recycler_view, parent, false);
        return new BetRecycler.ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, List<Object> payloads) {
        if(!payloads.isEmpty()) {
            if (payloads.get(0) instanceof String) {
                holder.monthMonthTotal.setText((String)payloads.get(0));
            }
        }else {
            super.onBindViewHolder(holder,position, payloads);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BetRecycler.ViewHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        holder.monthMonth.setText(getMonth(months.get(pos)));
        ArrayList<Bet> bets = getBetsForMonth(months.get(pos));
        int total = getTotal(bets);
        holder.monthMonthTotal.setText(String.valueOf(total));

        RecyclerView.LayoutManager manager = new LinearLayoutManager(holder.recyclerView.getContext());
        holder.recyclerView.setLayoutManager(manager);
        BetItemsAdapter adapter = new BetItemsAdapter(bets, databaseHelper);
        adapter.setClickListener((view, position1) -> {
            if(getBetsForMonth(months.get(pos)).isEmpty()) {
                months = databaseHelper.getMonths(bankroll);
                notifyDataSetChanged();
            } else {
                int i = getTotal(getBetsForMonth(months.get(pos)));
                notifyItemChanged(pos,String.valueOf(i));
            }
            callback.updateAccount();
        });
        holder.recyclerView.setAdapter(adapter);

        if (currentPosition == pos) {
            expand(holder.recyclerView);
            rotate(holder.monthExpandImage, 0f,180f);
        }
        Handler handler = new Handler();
        holder.monthExpandImage.setOnClickListener(view -> {
            currentPosition = pos;
            toggle(holder.recyclerView, holder.monthExpandImage);
        });

    }

    @Override
    public int getItemCount() {
        if(!months.isEmpty()) return months.size();
        else return 0;
    }

    private int getTotal(ArrayList<Bet> bets) {
        int i = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                i += (bet.getAmount() * bet.getOdd());
            }
            else if(bet.getProfit() < 0) i -= bet.getAmount();
            else i -= bet.getAmount();
        }
        return i;
    }

    private ArrayList<Bet> getBetsForMonth(String month) {
      return databaseHelper.getAllBetsForMonth(month, bankroll);
    }

    private void toggle(RecyclerView recyclerView, ImageView iv) {
        if(recyclerView.getVisibility() == View.GONE){
            rotate(iv,0f,180f);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.startAnimation(showAnimation);
        }
        else {
            rotate(iv,180f,0f);
            recyclerView.setVisibility(View.GONE);
            currentPosition = -1;
            recyclerView.startAnimation(hideAnimation);
        }
    }

    private void expand(RecyclerView recyclerView) {
        if(recyclerView.getVisibility() == View.GONE){
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void setCallback(BetRecycler.Callback callback) {
        this.callback = callback;
    }

    private String getMonth(String data) {
        SimpleDateFormat formatIn = new SimpleDateFormat("yyyy MM",Locale.getDefault());
        SimpleDateFormat formatOut = new SimpleDateFormat("MMMM yy",new Locale(LocaleHelper.getLanguage(context)));
        try {
            Date date = formatIn.parse(data);
            return formatOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
    private void rotate(ImageView v,float from, float to){
        RotateAnimation rotateAnimation = new RotateAnimation(from,to,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        rotateAnimation.setDuration(400);
        rotateAnimation.setFillAfter(true);
        v.startAnimation(rotateAnimation);
    }

}
