package com.example.betnotes.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class AppSettings {
    private static String currency = "";
    private static String currentBankroll = "";

    public static void loadSettings(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        AppSettings.currency = sharedpreferences.getString("currency","₽");
    }

    public static String getCurrency() {
        if(currency.isEmpty()) currency = "₽";
        return currency;
    }

    public static void setCurrency(Context context, String currency) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("currency", currency);
        editor.apply();
        AppSettings.currency = currency;
    }

    public static String getCurrentBankroll() {
        return currentBankroll;
    }

    public static void setCurrentBankroll(String currentBankroll) {
        AppSettings.currentBankroll = currentBankroll;
    }

}
