package com.example.betnotes.utils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.betnotes.fragments.InputActivity;
import com.example.betnotes.fragments.InputBankrollFragment;
import com.example.betnotes.fragments.InputBetFragment;

public class InputTabAdapter extends FragmentStateAdapter {

    public InputTabAdapter(InputActivity activity) {
        super(activity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if(position == 0) return new InputBetFragment();
        else return new InputBankrollFragment();
    }

    @Override
    public int getItemCount() {
        return 2;
    }



}
