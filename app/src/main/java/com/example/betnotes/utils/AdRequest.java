package com.example.betnotes.utils;

import android.os.Handler;
import android.os.Looper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AdRequest {
    public interface Result {
        void handleResult(String response) throws JSONException;
        void handleError(String string);
    }
    public Result resultInterface = null;

    public void ask(String phpUrl, String locale){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        final JSONObject[] object = new JSONObject[1];
        executor.execute(new Runnable() {
            @Override
            public void run() {
                java.net.URL url;
                HttpURLConnection connection = null;
                try {
                    url = new URL(phpUrl + "?" + "locale=" + URLEncoder.encode(locale,"UTF-8"));
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("Content-Type", "application/json; utf-8");
                    connection.setRequestProperty("Accept-Charset",  "UTF-8");
                    connection.setDoInput(true);
                    connection.setRequestMethod("GET");

                    try(BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        object[0] = new JSONObject(response.toString());
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if(object[0] != null) {
                                resultInterface.handleResult(object[0].getString("url"));
                            }
                        } catch (JSONException e) {
                            resultInterface.handleError("Server error 1");
                        }
                    }
                });
            }
        });
    }

}