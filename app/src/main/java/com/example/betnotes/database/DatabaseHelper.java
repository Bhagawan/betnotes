package com.example.betnotes.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bets_db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Bet.CREATE_TABLE);
        db.execSQL(Bankroll.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Bet.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Bankroll.TABLE_NAME);
        onCreate(db);
    }

    public void clearDb() {
        SQLiteDatabase db = getWritableDatabase();
        onUpgrade(db,1,1);
    }

    @SuppressLint("Range")
    public ArrayList<String> getBankrolls() {
        ArrayList<String> arrayList = new ArrayList<>();
        Boolean a;
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT " + Bankroll.COLUMN_NAME +" FROM " + Bankroll.TABLE_NAME;
        Cursor c = db.rawQuery(countQuery, null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                arrayList.add( c.getString( c.getColumnIndex(Bankroll.COLUMN_NAME)) );
                c.moveToNext();
            }
        }
        c.close();
        db.close();
        return arrayList;
    }

    @SuppressLint("Range")
    public ArrayList<String> getMonths(String bankroll) {
        ArrayList<String> arrayList = new ArrayList<>();
        boolean a;
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT strftime('%Y %m', " + Bet.COLUMN_TIMESTAMP +
                ") AS "+ Bet.COLUMN_TIMESTAMP + " FROM " + Bet.TABLE_NAME +" WHERE "+ Bet.COLUMN_BANKROLL +"=" + "'" + bankroll + "'"+ " ORDER BY " +
                Bet.COLUMN_TIMESTAMP + " DESC";
        Cursor c = db.rawQuery(countQuery, null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                if(arrayList.isEmpty())
                    arrayList.add( c.getString( c.getColumnIndex(Bet.COLUMN_TIMESTAMP)) );
                else {
                    String b = c.getString( c.getColumnIndex(Bet.COLUMN_TIMESTAMP));
                    a = true;
                    for (String string: arrayList) {
                        if(string.equals(b)) a = false;
                    }
                    if (a) arrayList.add(b);
                }
                a = false;
                c.moveToNext();
            }
        }
        c.close();
        db.close();
        return arrayList;
    }

    @SuppressLint("Range")
    public ArrayList<Bet> getAllBetsForMonth(String yearAndMonth, String bankroll) {
        ArrayList<Bet> arrayList = new ArrayList<>();
        Boolean a;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Bet.TABLE_NAME + " WHERE strftime('%Y %m'," + Bet.COLUMN_TIMESTAMP + ") = '" + yearAndMonth +"'" +
                " AND " + Bet.COLUMN_BANKROLL +"= " + "'" + bankroll + "'" + " ORDER BY " +
                Bet.COLUMN_TIMESTAMP + " DESC";
        try (Cursor cursor = db.rawQuery(selectQuery, null)) {
            if (null != cursor && cursor.moveToFirst()) {
                do {
                    Bet bet = new Bet();
                    bet.setId(cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_ID)));
                    bet.setBankroll(cursor.getString(cursor.getColumnIndex(Bet.COLUMN_BANKROLL)));
                    bet.setTimestamp(cursor.getString(cursor.getColumnIndex(Bet.COLUMN_TIMESTAMP)));
                    bet.setAmount(cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_AMOUNT)));
                    bet.setOdd(cursor.getFloat(cursor.getColumnIndex(Bet.COLUMN_ODD)));
                    bet.setName(cursor.getString(cursor.getColumnIndex(Bet.COLUMN_NAME)));
                    bet.setProfit(cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_PROFIT)));
                    arrayList.add(bet);
                } while (cursor.moveToNext());
            }
        }

        db.close();
        return arrayList;
    }

    @SuppressLint("Range")
    public void deleteBankroll(String bankroll) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ Bet.TABLE_NAME + " WHERE "+ Bet.COLUMN_BANKROLL +
                "=" + "'" + bankroll + "'");
        db.execSQL("delete from "+ Bankroll.TABLE_NAME + " WHERE "+ Bankroll.COLUMN_NAME +
                "=" + "'" + bankroll + "'");
        db.close();
    }

    public long addBet(String bankroll, String name, float odd, int amount, int profit) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Bet.COLUMN_BANKROLL, bankroll);
        values.put(Bet.COLUMN_NAME, name);
        values.put(Bet.COLUMN_ODD, odd);
        values.put(Bet.COLUMN_AMOUNT, amount);
        values.put(Bet.COLUMN_PROFIT,profit);

        long id = db.insert(Bet.TABLE_NAME, null, values);

        db.close();
        return id;
    }

    public long addBankroll(String bankroll, int amount) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Bankroll.COLUMN_NAME, bankroll);
        values.put(Bankroll.COLUMN_AMOUNT, amount);

        long id = db.insert(Bankroll.TABLE_NAME, null, values);

        db.close();
        return id;
    }

    public Bet getBet(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Bet.TABLE_NAME,
                new String[]{Bet.COLUMN_ID, Bet.COLUMN_BANKROLL, Bet.COLUMN_TIMESTAMP, Bet.COLUMN_NAME, Bet.COLUMN_ODD, Bet.COLUMN_AMOUNT},
                Bet.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        @SuppressLint("Range") Bet bet = new Bet(
                cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Bet.COLUMN_BANKROLL)),
                cursor.getString(cursor.getColumnIndex(Bet.COLUMN_TIMESTAMP)),
                cursor.getString(cursor.getColumnIndex(Bet.COLUMN_NAME)),
                cursor.getFloat(cursor.getColumnIndex(Bet.COLUMN_ODD)),
                cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_AMOUNT)),
                cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_PROFIT)));

        cursor.close();
        db.close();
        return bet;
    }
    public Bankroll getBankroll(String name) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Bankroll.TABLE_NAME,
                new String[]{Bankroll.COLUMN_ID, Bankroll.COLUMN_NAME, Bankroll.COLUMN_AMOUNT},
                Bankroll.COLUMN_NAME + "=?",
                new String[]{name}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        @SuppressLint("Range") Bankroll bankroll = new Bankroll(
                cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Bet.COLUMN_NAME)),
                cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_AMOUNT)));

        cursor.close();
        db.close();
        return bankroll;
    }

    @SuppressLint("Range")
    public ArrayList<Bet> getAllNotes(String bankroll) {
        ArrayList<Bet> bets = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + Bet.TABLE_NAME + " WHERE "+ Bet.COLUMN_BANKROLL +
                " = " + "'" + bankroll + "'" + " ORDER BY " +
                Bet.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();

        try (Cursor cursor = db.rawQuery(selectQuery, null)) {
            if (null != cursor && cursor.moveToFirst()) {
                do {
                    Bet bet = new Bet();
                    bet.setId(cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_ID)));
                    bet.setBankroll(cursor.getString(cursor.getColumnIndex(Bet.COLUMN_BANKROLL)));
                    bet.setTimestamp(cursor.getString(cursor.getColumnIndex(Bet.COLUMN_TIMESTAMP)));
                    bet.setAmount(cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_AMOUNT)));
                    bet.setOdd(cursor.getFloat(cursor.getColumnIndex(Bet.COLUMN_ODD)));
                    bet.setName(cursor.getString(cursor.getColumnIndex(Bet.COLUMN_NAME)));
                    bet.setProfit(cursor.getInt(cursor.getColumnIndex(Bet.COLUMN_PROFIT)));
                    bets.add(bet);
                } while (cursor.moveToNext());
            }
        }


        db.close();

        return bets;
    }

    public int getBetCount(String bankroll) {
        String countQuery = "SELECT  * FROM " + Bet.TABLE_NAME +" WHERE "+ Bet.COLUMN_BANKROLL +
                "=" + "'" +bankroll + "'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public int updateBet(Bet bet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Bet.COLUMN_BANKROLL, bet.getBankroll());
        values.put(Bet.COLUMN_NAME, bet.getName());
        values.put(Bet.COLUMN_ODD, bet.getOdd());
        values.put(Bet.COLUMN_AMOUNT, bet.getAmount());
        values.put(Bet.COLUMN_PROFIT, bet.getProfit());

        return db.update(Bet.TABLE_NAME, values, Bet.COLUMN_ID + " = ?",
                new String[]{String.valueOf(bet.getId())});
    }

    public int updateBankroll(Bankroll bankroll) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Bet.COLUMN_NAME, bankroll.getName());
        values.put(Bet.COLUMN_AMOUNT, bankroll.getAmount());

        return db.update(Bankroll.TABLE_NAME, values, Bankroll.COLUMN_ID + " = ?",
                new String[]{String.valueOf(bankroll.getId())});
    }


    public void deleteBet(Bet bet) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Bet.TABLE_NAME, Bet.COLUMN_ID + " = ?", new String[]{String.valueOf(bet.getId())});
        db.close();
    }


}
