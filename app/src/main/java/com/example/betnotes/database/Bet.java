package com.example.betnotes.database;

public class Bet {
    public static final String TABLE_NAME = "bets";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_BANKROLL = "bankroll";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ODD = "odd";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_PROFIT = "profit";

    private int id, amount, profit;
    private float odd;
    private String bankroll, name, timestamp;

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_BANKROLL + " TEXT,"
                    + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_ODD + " REAL,"
                    + COLUMN_AMOUNT + " INTEGER,"
                    + COLUMN_PROFIT + " INTEGER"
                    + ")";
    public Bet() {}

    public Bet(int id, String bankroll, String date, String name, Float odd, int amount, int profit) {
        this.amount = amount;
        this.bankroll = bankroll;
        this.id = id;
        this.odd = odd;
        this.name = name;
        this.timestamp = date;
        this.profit =profit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getOdd() {
        return odd;
    }

    public void setOdd(float odd) {
        this.odd = odd;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBankroll() {
        return bankroll;
    }

    public void setBankroll(String bankroll) {
        this.bankroll = bankroll;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }
}
