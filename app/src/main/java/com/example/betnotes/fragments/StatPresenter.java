package com.example.betnotes.fragments;

import android.content.Context;

import com.example.betnotes.R;
import com.example.betnotes.database.Bet;
import com.example.betnotes.database.DatabaseHelper;
import com.example.betnotes.utils.AppSettings;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class StatPresenter extends MvpPresenter<StatPresenterViewInterface> {
    private DatabaseHelper database ;
    private Context context;

    public void setDefault(Context context) {
        database = new DatabaseHelper(context);
        this.context = context;
        ArrayList<String> b = database.getBankrolls();
        if(!b.isEmpty()) {
            if(AppSettings.getCurrentBankroll().isEmpty()) AppSettings.setCurrentBankroll(b.get(0));
            getViewState().setMenu(database.getBankrolls());
        }
    }

    public void updateData() {
        String bankroll = AppSettings.getCurrentBankroll();
        setGraph(bankroll);
        setTotal(bankroll);
        setSuccess(bankroll);
        setBetAmount(bankroll);
        setBenefit(bankroll);
        setROC(bankroll);
        setROI(bankroll);
        setAvBet(bankroll);
        setAvOdd(bankroll);
    }

    private void setGraph(String bankroll) {
        ArrayList<Bet> bets = database.getAllNotes(bankroll);
        ArrayList<Entry> entries = new ArrayList<>();
            int total =  database.getBankroll(bankroll).getAmount();
            int profit = 0;
            int benefit = 0;
            entries.add(new Entry(0f, (float) total));
            for(int i = 0; i < bets.size(); i++) {
                total -= bets.get(i).getAmount();
                profit = bets.get(i).getProfit();
                benefit = (int) (bets.get(i).getAmount() * bets.get(i).getOdd());
                if(profit < 0) total -= benefit;
                else if(profit > 0) total += benefit;
                entries.add(new Entry(i+1, total));
            }
        getViewState().setGraph(entries);
    }

    private void setTotal(String bankroll) {
        String total ="";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);
        int start = database.getBankroll(bankroll).getAmount();

        int i = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                i += (bet.getAmount() * bet.getOdd());
            }
            else if(bet.getProfit() < 0) i -= (bet.getAmount() * bet.getOdd());
            else i -= bet.getAmount();
        }
        total = String.valueOf(i + start) + AppSettings.getCurrency();
        getViewState().setTotal(total);
    }

    private void setBenefit(String bankroll) {
        String benefit ="";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);

        int i = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                i += (bet.getAmount() * bet.getOdd());
            }
            else if(bet.getProfit() < 0) i -= (bet.getAmount() * bet.getOdd());
            else i -= bet.getAmount();
        }
        benefit = i + AppSettings.getCurrency();
        getViewState().setBenefit(benefit);
    }

    private  void setBetAmount(String bankroll) {
        String amount ="";
        int a = database.getAllNotes(bankroll).size();
        ArrayList<Bet> bets = database.getAllNotes(bankroll);
        int b = 0;
        int h = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() == 0) {
                h++;
            }
            else b++;
        }
        if( h == 0) amount = String.valueOf(b);
        else amount = b + " (" + h + " " + context.getResources().getString(R.string.in_process) +
                ")";
        getViewState().setBetAmount(amount);
    }

    private void setSuccess(String bankroll) {
        String success ="";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);
        int s = 0;
        int f = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                s++;
            }
            else if(bet.getProfit() < 0) f++;
        }
        float p ;
        if (f == 0) p = 100;
        else p = (float)(s * 100) / (s + f);
        success = String.valueOf(p) +"%";
        getViewState().setSuccess(success);
    }

    private void setROI(String bankroll) {
        String roi ="0";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);
        int start = database.getBankroll(bankroll).getAmount();

        int i = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                i += (bet.getAmount() * bet.getOdd());
            }
            else if(bet.getProfit() < 0) i -= (bet.getAmount() * bet.getOdd());
            else i -= bet.getAmount();
        }
        if(start == 0) roi = context.getString(R.string.err_zero_inv);
        else roi = (i * 100) / start +"%";
        getViewState().setROI(roi);
    }

    private void setROC(String bankroll) {
        String roc ="";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);
        int tot = database.getBankroll(bankroll).getAmount();
        int in = 0;
        int out = 0;

        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                in += (bet.getAmount() * bet.getOdd());
                tot += (bet.getAmount() * bet.getOdd());
            }
            else if(bet.getProfit() < 0) {
                out += (bet.getAmount() * bet.getOdd());
                tot -= (bet.getAmount() * bet.getOdd());
            }
            else tot += bet.getAmount();

        }
        if(tot == 0) roc = context.getString(R.string.err_zero_inv);
        else roc = ((in - out) *100 )/ tot +"%";
        getViewState().setROC(roc);
    }

    private void setAvBet(String bankroll) {
        String avBet ="";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);

        int total = 0;
        for(Bet bet: bets) {
            total += bet.getAmount();
        }
        if(bets.size() == 0) avBet = "0";
        else avBet = total / bets.size() + AppSettings.getCurrency();
        getViewState().setAvBet(avBet);
    }

    private void setAvOdd(String bankroll) {
        String avOdd ="";
        ArrayList<Bet> bets = database.getAllNotes(bankroll);

        float total = 0;
        for(Bet bet: bets) {
            total += bet.getOdd();
        }
        if(bets.size() == 0) avOdd = "0";
        else avOdd = String.valueOf(total / bets.size()) ;
        getViewState().setAvOdd(avOdd);
    }
}
