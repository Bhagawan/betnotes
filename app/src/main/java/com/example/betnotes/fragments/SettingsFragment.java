package com.example.betnotes.fragments;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.betnotes.R;
import com.example.betnotes.database.Bankroll;
import com.example.betnotes.database.Bet;
import com.example.betnotes.database.DatabaseHelper;
import com.example.betnotes.utils.AppSettings;
import com.example.betnotes.utils.LocaleHelper;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class SettingsFragment extends Fragment {
    private  View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        setupLanguageSwitch();
        setupCurrencySwitch();
        setupDeleteButton();

        return view;
    }

    private void setupLanguageSwitch() {
        TabLayout tabLayout = view.findViewById(R.id.settings_language);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.drawable.ic_ru);
        tabLayout.getTabAt(0).setCustomView(imageView);

        ImageView imageView2 = new ImageView(getContext());
        imageView2.setImageResource(R.drawable.ic_gb);
        tabLayout.getTabAt(1).setCustomView(imageView2);

        switch(LocaleHelper.getLanguage(getContext())) {
            case "ru" :
                tabLayout.getTabAt(0).select();
                break;
            case "en" :
                tabLayout.getTabAt(1).select();
                break;
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        loadSettings("ru");
                        break;
                    case 1:
                        loadSettings("en");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void loadSettings(String lang) {
        LocaleHelper.setLocale(getContext(),lang);
        androidx.appcompat.widget.AppCompatButton button = view.findViewById(R.id.settings_button);
        button.setText(getResources().getString(R.string.delete_bankroll));
    }

    private void setupCurrencySwitch() {

        TabLayout tabLayout = view.findViewById(R.id.settings_currency);

        AppSettings.loadSettings(getContext());
        switch(AppSettings.getCurrency()) {
            case "₽":
                tabLayout.getTabAt(0).select();
                break;
            case "$" :
                tabLayout.getTabAt(1).select();
                break;
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTo("₽");
                        break;
                    case 1:
                        switchTo("$");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void switchTo(String currency) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View currAlert = getLayoutInflater().inflate(R.layout.currency_rate, null);
        TextView header = currAlert.findViewById(R.id.currency_rate_header);
        String h ;
        if(currency.equals("₽")) h = "$" + " =";
        else h ="₽" + " =";
        header.setText(h);
        TextView footer = currAlert.findViewById(R.id.currency_rate_footer);
        footer.setText(currency);
        EditText input = currAlert.findViewById(R.id.currency_rate_input);

        builder.setView(currAlert);

        builder.setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> {

        });
        builder.setPositiveButton(getResources().getString(R.string.ok), (dialog, which) -> {
            try {
                AppSettings.setCurrency(getContext(), currency);
                translateDatabaseToCurrency(Float.parseFloat(input.getText().toString()));
            }
            catch (NumberFormatException e) {
                Toast.makeText(getContext(), "01",Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void translateDatabaseToCurrency(float multiplier) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        ArrayList<String> bankrolls = databaseHelper.getBankrolls();
        if(!bankrolls.isEmpty()) {
            for(String bankroll: bankrolls) {
                Bankroll b = databaseHelper.getBankroll(bankroll);
                b.setAmount((int)(b.getAmount() * multiplier));
                databaseHelper.updateBankroll(b);
                ArrayList<Bet> bets = databaseHelper.getAllNotes(bankroll);
                for(Bet bet: bets) {
                    bet.setAmount((int)(bet.getAmount() * multiplier));
                    databaseHelper.updateBet(bet);
                }
            }
        }
    }

    private void setupDeleteButton() {
        androidx.appcompat.widget.AppCompatButton button = view.findViewById(R.id.settings_button);
        button.setOnClickListener(v -> {
            DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
            String bankroll = AppSettings.getCurrentBankroll();
            if(!bankroll.isEmpty()) {
                databaseHelper.deleteBankroll(bankroll);
                AppSettings.setCurrentBankroll("");
                Toast.makeText(getContext(),getString(R.string.bankroll_deleted),Toast.LENGTH_SHORT).show();
            }
            else Toast.makeText(getContext(),getString(R.string.no_bancrolls_selected),Toast.LENGTH_SHORT).show();
        });
    }


}