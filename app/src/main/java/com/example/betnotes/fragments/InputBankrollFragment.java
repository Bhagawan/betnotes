package com.example.betnotes.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.betnotes.R;
import com.example.betnotes.database.DatabaseHelper;


public class InputBankrollFragment extends Fragment {
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_input_bankroll, container, false);
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        EditText nameInput = view.findViewById(R.id.input_bankroll_name_input);
        EditText amountInput = view.findViewById(R.id.input_bankroll_amount_input);

        androidx.appcompat.widget.AppCompatButton inputButton = view.findViewById(R.id.input_bankroll_input_button);
        inputButton.setOnClickListener(v -> {
            if(nameInput.getText().toString().isEmpty() || amountInput.getText().toString().isEmpty())
                Toast.makeText(getContext(), getResources().getString(R.string.input_error), Toast.LENGTH_SHORT).show();
            else {
                databaseHelper.addBankroll(nameInput.getText().toString(),Integer.parseInt(amountInput.getText().toString()));
                getActivity().finish();
            }
        });

        return view;
    }

}