package com.example.betnotes.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.betnotes.R;
import com.example.betnotes.database.DatabaseHelper;
import com.example.betnotes.utils.AppSettings;
import com.example.betnotes.utils.BetRecycler;
import com.example.betnotes.utils.ImmSpin;
import com.example.betnotes.utils.SpinnerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class HomeFragment extends MvpAppCompatFragment implements HomePresenterInterfaceView{
    private  View view;
    private boolean isNotificationActive = false;

    @InjectPresenter
    public HomePresenter homePres;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        androidx.appcompat.widget.AppCompatSpinner spinner = view.findViewById(R.id.home_spinner);
        ImmSpin.avoidSpinnerDropdownFocus(spinner);

        FloatingActionButton button = view.findViewById(R.id.floatingActionButton);
        button.setOnClickListener(v -> {
            Intent i = new Intent(getContext(),InputActivity.class);
            getContext().startActivity(i);
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        homePres.openDatabase(getContext());
    }


    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setMenu(ArrayList<String> bankrolls) {
        androidx.appcompat.widget.AppCompatSpinner spinner = view.findViewById(R.id.home_spinner);

        spinner.setFocusable(false);
        SpinnerAdapter adapter = new SpinnerAdapter(spinner.getContext(),bankrolls);
        spinner.setAdapter(adapter);

        spinner.setSelection(bankrolls.indexOf(AppSettings.getCurrentBankroll()));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppSettings.setCurrentBankroll(parent.getItemAtPosition(position).toString());
                homePres.showBets(getContext());
                homePres.updateAccount();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void updateBets(DatabaseHelper databaseHelper) {
        TextView account =view.findViewById(R.id.bankroll_account);
        account.setVisibility(View.VISIBLE);
        if(isNotificationActive) {
            LinearLayout linearLayout = view.findViewById(R.id.home_linear_layout);
            LinearLayout textCard = linearLayout.findViewById(R.id.text_card_layout);
            linearLayout.removeView(textCard);
            isNotificationActive = false;
        }
        RecyclerView recyclerView = view.findViewById(R.id.home_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        BetRecycler adapter = new BetRecycler(getContext(),AppSettings.getCurrentBankroll());
        adapter.setCallback(() -> {
            homePres.check();
            homePres.updateAccount();
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateAccount(String amount) {
        TextView account = view.findViewById(R.id.bankroll_account);
        account.setText(amount);
    }

    @Override
    public void noBankrollsMessage() {
        TextView account =view.findViewById(R.id.bankroll_account);
        account.setVisibility(View.GONE);
        LinearLayout linearLayout = view.findViewById(R.id.home_linear_layout);
        if(isNotificationActive) {
            TextView message = linearLayout.findViewById(R.id.notification_message);
            message.setText(getResources().getString(R.string.no_bankrolls));
        }
        else {
            LinearLayout textCard = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.text_card, null);
            TextView message = textCard.findViewById(R.id.notification_message);
            message.setText(getResources().getString(R.string.no_bankrolls));
            linearLayout.addView(textCard, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
        }
       isNotificationActive = true;
    }

    @Override
    public void noBetsMessage() {
        TextView account = view.findViewById(R.id.bankroll_account);
        account.setVisibility(View.VISIBLE);
        LinearLayout linearLayout = view.findViewById(R.id.home_linear_layout);
        if(isNotificationActive) {
            TextView message = linearLayout.findViewById(R.id.notification_message);
            message.setText(getResources().getString(R.string.no_bets));
        }
        else {
            LinearLayout textCard = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.text_card, null);
            TextView message = textCard.findViewById(R.id.notification_message);
            message.setText(getResources().getString(R.string.no_bets));
            linearLayout.addView(textCard, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
        }
        isNotificationActive = true;
    }

}