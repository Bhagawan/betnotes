package com.example.betnotes.fragments;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.betnotes.R;
import com.example.betnotes.database.DatabaseHelper;

import java.util.ArrayList;

public class InputBetFragment extends Fragment {
    private View view;
    private String bankroll;
    private DatabaseHelper databaseHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_input_bet, container, false);
        bankroll = getResources().getString(R.string.bankroll);
        databaseHelper = new DatabaseHelper(getContext());

        Button button = view.findViewById(R.id.input_bet_spinner_button);
        button.setOnClickListener(v -> createDial());
        EditText nameInput = view.findViewById(R.id.input_bet_name_input);
        EditText oddInput = view.findViewById(R.id.input_bet_odd_input);
        EditText amountInput = view.findViewById(R.id.input_bet_amount_input);
        androidx.appcompat.widget.AppCompatButton inputButton = view.findViewById(R.id.input_bet_input_button);
        inputButton.setOnClickListener(v -> {
            try{
                String name = nameInput.getText().toString();
                float odd = Float.parseFloat(oddInput.getText().toString());
                int amount = Integer.parseInt(amountInput.getText().toString());
                if(bankroll.equals(getResources().getString(R.string.bankroll)) || name.isEmpty() ||
                        oddInput.getText().toString().isEmpty() || amountInput.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), getResources().getString(R.string.input_error), Toast.LENGTH_SHORT).show();
                }
                else {
                    databaseHelper.addBet(bankroll, name, odd, amount, 0);
                    getActivity().finish();
                }
            }
            catch (NumberFormatException exception) {
                Toast.makeText(getContext(), getResources().getString(R.string.input_error), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private void createDial() {
        ArrayList<String> bankrolls = databaseHelper.getBankrolls();
        if(!bankrolls.isEmpty()){
            String[] strings = bankrolls.toArray(new String[bankrolls.size()]);
            final String[] selectedBankroll = {strings[0]};

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setNegativeButton(getResources().getString(R.string.cancel), null);
            builder.setPositiveButton(getResources().getString(R.string.ok), (dialog, which) -> {
                selectBankroll(selectedBankroll[0]);
                dialog.dismiss();
            });
            int c;
            builder.setSingleChoiceItems(strings,0 , (dialog, which) -> selectedBankroll[0] = strings[which]);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        else Toast.makeText(getContext(),getResources().getString(R.string.input_error_null_bank),Toast.LENGTH_SHORT).show();
    }

    private void selectBankroll(String bankroll) {
        Button button = view.findViewById(R.id.input_bet_spinner_button);
        button.setText(bankroll);
        this.bankroll = bankroll;
    }
}