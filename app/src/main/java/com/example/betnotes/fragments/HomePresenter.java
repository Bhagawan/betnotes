package com.example.betnotes.fragments;

import android.content.Context;

import com.example.betnotes.database.Bet;
import com.example.betnotes.database.DatabaseHelper;
import com.example.betnotes.utils.AppSettings;

import java.util.ArrayList;
import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomePresenterInterfaceView> {
    private DatabaseHelper database;


    public void openDatabase(Context context) {
        if (database == null) database = new DatabaseHelper(context);
        ArrayList<String> b = database.getBankrolls();
        if (!b.isEmpty()){
            if(AppSettings.getCurrentBankroll().isEmpty()) AppSettings.setCurrentBankroll(b.get(0));
            getViewState().setMenu(b);
            update(AppSettings.getCurrentBankroll());
            updateAccount();
        }
        else {
            getViewState().noBankrollsMessage();
        }
    }

    private void update(String bankroll) {
        if(database.getAllNotes(bankroll).isEmpty()){
            getViewState().noBetsMessage();
        }
        else {
            getViewState().updateBets(database);
        }
    }

    public void check() {
        String bankroll = AppSettings.getCurrentBankroll();
        if(database.getAllNotes(bankroll).isEmpty()){
            getViewState().noBetsMessage();
        }
    }

    private boolean existBankroll(String bankroll) {
        ArrayList<String> bankrolls = database.getBankrolls();
        for(String name: bankrolls) {
            if(name.equals(bankroll)) return true;
        }
        return false;
    }

    public void updateAccount() {
        String bankroll = AppSettings.getCurrentBankroll();
        int account = database.getBankroll(bankroll).getAmount();
        int c = getTotal(database.getAllNotes(bankroll));
        String amount = String.valueOf(account + c);
        getViewState().updateAccount(amount  + AppSettings.getCurrency());
    }

    public void showBets(Context context) {
        if (database == null) database = new DatabaseHelper(context);
        String bankroll = AppSettings.getCurrentBankroll();
        if(database.getAllNotes(bankroll).isEmpty()){
            getViewState().updateBets(database);
            getViewState().noBetsMessage();
            updateAccount();
        }
        else
            getViewState().updateBets(database);
    }
    public void clear() {
        database.clearDb();
    }

    private int getTotal(List<Bet> bets) {
        int i = 0;
        for(Bet bet: bets) {
            if(bet.getProfit() > 0) {
                i += (bet.getAmount() * bet.getOdd());
            }
            else if(bet.getProfit() < 0) i -= bet.getAmount();
            else i -= bet.getAmount();
        }
        return i;
    }
}
