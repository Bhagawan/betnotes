package com.example.betnotes.fragments;

import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.example.betnotes.R;
import com.example.betnotes.utils.AppSettings;
import com.example.betnotes.utils.ImmSpin;
import com.example.betnotes.utils.SpinnerAdapter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class StatFragment extends MvpAppCompatFragment implements StatPresenterViewInterface{
    private  View view;

    @InjectPresenter
    public StatPresenter statPres;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_stat, container, false);
        statPres.setDefault(getContext());
        androidx.appcompat.widget.AppCompatSpinner spinner = view.findViewById(R.id.stat_spinner);
        ImmSpin.avoidSpinnerDropdownFocus(spinner);
        LineChart lineChart = view.findViewById(R.id.chart);
        lineChart.setNoDataText("");
        lineChart.invalidate();
        return view;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void setMenu(ArrayList<String> bankrolls) {
        androidx.appcompat.widget.AppCompatSpinner spinner = view.findViewById(R.id.stat_spinner);

        spinner.setFocusable(false);
        SpinnerAdapter adapter = new SpinnerAdapter(spinner.getContext(),bankrolls);
        spinner.setAdapter(adapter);
        spinner.setSelection(bankrolls.indexOf(AppSettings.getCurrentBankroll()));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppSettings.setCurrentBankroll(parent.getItemAtPosition(position).toString());
                statPres.updateData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void setGraph(ArrayList<Entry> entries) {
        LineChart lineChart = view.findViewById(R.id.chart);

        LineDataSet dataset = new LineDataSet(entries, "");
        dataset.setDrawCircles(false);
        dataset.setDrawFilled(true);
        dataset.setDrawValues(false);
        dataset.setFillColor(getResources().getColor(R.color.chart_fill));

        LineData data = new LineData(dataset);
        lineChart.setData(data);
        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.getXAxis().setEnabled(false);


        lineChart.getAxisRight().setEnabled(false);
        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setDrawZeroLine(true);
        leftAxis.setTextColor(getResources().getColor(R.color.grey));
        leftAxis.setZeroLineColor(getResources().getColor(R.color.grey));
        leftAxis.setGranularity(100);

        lineChart.invalidate();
    }

    @Override
    public void setTotal(String total) {
        TextView textView = view.findViewById(R.id.stat_total);
        textView.setText(total);
    }

    @Override
    public void setBenefit(String total) {
        TextView textView = view.findViewById(R.id.stat_benefit);
        textView.setText(total);
    }

    @Override
    public void setBetAmount(String total) {
        TextView textView = view.findViewById(R.id.stat_bet_number);
        textView.setText(total);
    }

    @Override
    public void setSuccess(String total) {
        TextView textView = view.findViewById(R.id.stat_success);
        textView.setText(total);
    }

    @Override
    public void setROI(String total) {
        TextView textView = view.findViewById(R.id.stat_roi);
        textView.setText(total);
    }

    @Override
    public void setROC(String total) {
        TextView textView = view.findViewById(R.id.stat_roc);
        textView.setText(total);
    }

    @Override
    public void setAvBet(String total) {
        TextView textView = view.findViewById(R.id.stat_av_bet);
        textView.setText(total);
    }

    @Override
    public void setAvOdd(String total) {
        TextView textView = view.findViewById(R.id.stat_av_odd);
        textView.setText(total);
    }

}