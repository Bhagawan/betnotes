package com.example.betnotes.fragments;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface StatPresenterViewInterface extends MvpView {
    @AddToEnd
    void showMessage(String message);

    @AddToEnd
    void setMenu(ArrayList<String> bankrolls);

    @AddToEnd
    void setGraph(ArrayList<Entry> entries);

    @AddToEnd
    void setTotal(String total);

    @AddToEnd
    void setBenefit(String total);

    @AddToEnd
    void setBetAmount(String total);

    @AddToEnd
    void setSuccess(String total);

    @AddToEnd
    void setROI(String total);

    @AddToEnd
    void setROC(String total);

    @AddToEnd
    void setAvBet(String total);

    @AddToEnd
    void setAvOdd(String total);

}
