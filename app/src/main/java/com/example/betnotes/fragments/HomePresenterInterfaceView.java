package com.example.betnotes.fragments;

import com.example.betnotes.database.DatabaseHelper;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface HomePresenterInterfaceView extends MvpView {
    @AddToEnd
    void showMessage(String message);

    @AddToEnd
    void setMenu(ArrayList<String> bankrolls);

    @AddToEnd
    void updateBets(DatabaseHelper databaseHelper);

    @AddToEnd
    void updateAccount(String amount);

    @AddToEnd
    void noBankrollsMessage();

    @AddToEnd
    void noBetsMessage();
}
